# Kubernetes Workshop
Lab 05: Deploying applications using Ingress

---

## Instructions

### Create a directory for the lab

 - Create a new directory to store the required files
```
mkdir ~/lab-05
cd ~/lab-05
```

### Install the Nginx Ingress Controller

 - Install the nginx ingress controller by apply the following
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/provider/cloud-generic.yaml
```

### Verify the installation

 - Verify installation 
```
kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx --watch
```

### Deploy the application

 - Inspect the "cats", "dogs" and "birds" application (pods and ClusterIP services)
```
curl https://gitlab.com/sela-aks-workshop/lab-05/raw/master/cats.yaml
```
```
curl https://gitlab.com/sela-aks-workshop/lab-05/raw/master/dogs.yaml
```
```
curl https://gitlab.com/sela-aks-workshop/lab-05/raw/master/birds.yaml
```

 - Deploy the "cats" application
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-05/raw/master/cats.yaml
```

 - Deploy the "dogs" application

```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-05/raw/master/dogs.yaml
```

 - Deploy the "birds" application
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-05/raw/master/birds.yaml
```

### Deploy the ingress resource

 - Inspect the ingress resource
```
curl https://gitlab.com/sela-aks-workshop/lab-05/raw/master/ingress.yaml
```

 - Create the ingress resource
```
kubectl create -f https://gitlab.com/sela-aks-workshop/lab-05/raw/master/ingress.yaml
```

### Inspect the created resources

 - List existent pods
```
kubectl get pods
```

 - List existent services
```
kubectl get svc
```

 - List existent ingress 
```
kubectl get ingress
```

### Access the application

 - Get the ingress controller External IP 
```
kubectl get svc ingress-nginx -n ingress-nginx
```

 - Browse to the cats service
```
http://<ingress-controller-ip>/cats
```

 - Browse to the dogs service
```
http://<ingress-controller-ip>/dogs
```

 - Browse to the birds service
```
http://<ingress-controller-ip>/birds
```

### Cleanup

 - List existent resources
```
kubectl get all
```

 - Delete ingress resource
```
kubectl delete ingress --all
```

 - Delete existent resources
```
kubectl delete all --all
```

 - List existent resources
```
kubectl get all
```
